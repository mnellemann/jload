import org.redline_rpm.header.Os

buildscript {
    repositories {
        maven { url("https://plugins.gradle.org/m2/") }
    }
    dependencies {
        classpath 'org.redline-rpm:redline:1.2.10'
    }
}

plugins {
    id 'java'
    id 'application'
    id "com.github.johnrengelman.shadow" version "7.1.2"
    id "net.nemerosa.versioning" version "2.15.1"
    id "nebula.ospackage" version "9.1.1"
}

repositories {
    mavenCentral()
    mavenLocal()
}

dependencies {
    annotationProcessor 'info.picocli:picocli-codegen:4.7.3'
    implementation 'info.picocli:picocli:4.7.3'
    implementation 'org.slf4j:slf4j-api:2.0.7'
    runtimeOnly 'org.slf4j:slf4j-simple:2.0.7'
}

application {
    mainClass.set('biz.nellemann.jload.Application')
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

test {
    useJUnitPlatform()
}

jar {
    manifest {
        attributes(
            'Created-By'     : "Gradle ${gradle.gradleVersion}",
            'Build-OS'       : "${System.properties['os.name']} ${System.properties['os.arch']} ${System.properties['os.version']}",
            'Build-Jdk'      : "${System.properties['java.version']} (${System.properties['java.vendor']} ${System.properties['java.vm.version']})",
            'Build-User'     : System.properties['user.name'],
            'Build-Version'  : versioning.info.tag ?: (versioning.info.branch + "-" + versioning.info.build),
            'Build-Revision' : versioning.info.commit,
            'Build-Timestamp': new Date().format("yyyy-MM-dd'T'HH:mm:ss.SSSZ").toString(),
        )
    }
}

apply plugin: 'nebula.ospackage'
ospackage {
    packageName = 'jload'
    release = '1'
    user = 'root'
    packager = "Mark Nellemann <mark.nellemann@gmail.com>"

    into '/opt/jload'

    from(shadowJar.outputs.files) {
        into 'lib'
    }

    from('build/scriptsShadow') {
        into 'bin'
    }

    from(['README.md', 'LICENSE']) {
        into 'doc'
    }

}

buildDeb {
    dependsOn build, startShadowScripts
}

buildRpm {
    dependsOn build, startShadowScripts
    os = Os.LINUX
}

tasks.register("packages") {
    group "build"
    dependsOn ":buildDeb"
    dependsOn ":buildRpm"
}
