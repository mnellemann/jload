# TARGET can be x86, x86_64, ppc64le or aarch64
ARG TARGET=x86_64

# VERSION can be latest-stable, v3.13, v3.14, etc.
ARG VERSION=v3.14

# BASEIMAGE
ARG BASE_IMAGE=multiarch/alpine:${TARGET}-${VERSION}
FROM ${BASE_IMAGE}

# Add a layer with the required Java runtime
#RUN apt-get update && apt-get install -y default-jre-headless
RUN apk update && apk add openjdk11-jre-headless

# Add a layer with our application artifact
RUN mkdir -p /opt/jload
COPY build/libs/*-all.jar /opt/jload/jload.jar

# Instructions for running the container
USER nobody
WORKDIR /opt/jload
ENTRYPOINT ["/usr/bin/java", "-jar", "jload.jar"]
