/*
   Copyright 2020 mark.nellemann@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package biz.nellemann.jload;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.time.Duration;
import java.time.Instant;
import java.util.Random;

class DiskThread extends Thread {

    private final static Logger log = LoggerFactory.getLogger(DiskThread.class);

    private static final int AMPLIFIER = 50;


    final long iterations;
    final long weight;
    final int maxSleep = 100;
    final int minSleep = 0;

    DiskThread(final long iterations, final long weight) {
        this.iterations = iterations;
        this.weight = weight * 100;
    }


    @Override
    public void run() {

        double rampUpIterations = Math.min(iterations*0.2, maxSleep);
        double rampDownIterations = Math.min(iterations*0.2, maxSleep);
        long sleep = (long) Math.min(rampUpIterations, maxSleep);

        log.info("run() => rampUps: {}, rampDowns: {}, iterations: {}, weight: {}, sleep: {},", rampUpIterations, rampDownIterations, iterations, weight, sleep);

        for(int i = 0; i < iterations; i++) {

            if(i <= rampUpIterations) {
                log.debug("{} - Ramping Up", i);
                sleep = Math.max(sleep-1, minSleep);
            } else if (i >= (iterations - rampDownIterations)) {
                log.debug("{} - Ramping down", i);
                sleep = Math.min(sleep+1, maxSleep);
            } else {
                log.debug("{} - Full go", i);
                sleep=minSleep;
            }

            try {
                writeFixedData( (weight * AMPLIFIER) / Math.max(5,sleep), sleep);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }


    }


    protected void writeFixedData(long weight, long sleep) throws InterruptedException {
        Instant start = Instant.now();
        long tmpWritten = 0;
        try {
            File file = File.createTempFile("diskThread", ".txt");
            file.deleteOnExit();

            FileOutputStream fos = new FileOutputStream(file);
            OutputStreamWriter w = new OutputStreamWriter(fos, StandardCharsets.UTF_8);

            BufferedWriter bw = new BufferedWriter(w);
            for(int i = 0; i < weight; i++) {

                // Write a bunch of chars
                for(int x = 0; x < 65535; x++) {
                    w.append((char)x);
                }
                bw.flush();
                sleep(sleep);
            }

            bw.close();
            fos.close();
            tmpWritten = Files.size(file.toPath());

            //noinspection ResultOfMethodCallIgnored
            file.delete();

        } catch (IOException e) {
            e.printStackTrace();
        }

        Instant end = Instant.now();
        log.info("writeFixedData() - {} bytes in {} seconds - weight: {}, sleep: {}", tmpWritten, Duration.between(start, end).getSeconds(), weight, sleep);
    }


    protected void writeRandomData() {

        try {

            File file = File.createTempFile("diskThread", ".txt");
            file.deleteOnExit();

            FileOutputStream fos = new FileOutputStream(file);
            OutputStreamWriter w = new OutputStreamWriter(fos, StandardCharsets.UTF_8);
            BufferedWriter bw = new BufferedWriter(w);

            for (int i = 0; i < weight; i++) {

                // Build the data of semi-random 16bit unicode chars
                int random = (int) (Math.random() * 7) + 1;
                char[] data = new char[2048000];
                for(int n = 0; n < data.length; n++) {
                    data[n] = (char) (random + i);
                }

                // Write the data
                for (int x = 0; x < data.length; x++) {
                    w.append(data[x]);
                }
                bw.flush();

            }

            bw.close();
            fos.close();
            file.delete();

        } catch (final IOException e) {
            e.printStackTrace();
        }

    }

}
