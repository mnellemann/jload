/*
   Copyright 2020 mark.nellemann@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package biz.nellemann.jload;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine;
import picocli.CommandLine.Command;

import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.lang.Thread.sleep;

@Command(name = "jload",
    mixinStandardHelpOptions = true, sortOptions = false,
    description = "Generate random load on CPU and Disk.",
    versionProvider = biz.nellemann.jload.VersionProvider.class)
public class Application implements Callable<Integer> {

    private final static Logger log = LoggerFactory.getLogger(Application.class);

    @CommandLine.Option(names = {"-r", "--repeats"}, description = "Repetitions or -1 for continued (default: ${DEFAULT-VALUE}).", paramLabel = "num")
    private int numberOfRepeats = 10;

    @CommandLine.Option(names = {"-t", "--threads"}, description = "Threads to start (processor and disk) (default: ${DEFAULT-VALUE}).", paramLabel = "num")
    private int numberOfThreads = 2;

    @CommandLine.Option(names = {"-i", "--iterations"}, description = "Iterations for each thread (default: ${DEFAULT-VALUE}).", paramLabel = "num")
    private int numberOfIterations = 100;

    @CommandLine.Option(names = {"--no-processor"}, negatable = true, description = "Disable load on processor(s).")
    private boolean testProcessor = true;

    @CommandLine.Option(names = {"--no-disk"}, negatable = true, description = "Disable load on disk(s).")
    private boolean testDisk = true;

    @CommandLine.Option(names = {"-w", "--weight"}, description = "Weighted workload pr. iteration (default: ${DEFAULT-VALUE}).", paramLabel = "num")
    static long weight = 10;


    public static void main(String... args) {
        int exitCode = new CommandLine(new Application()).execute(args);
        System.exit(exitCode);
    }


    @Override
    public Integer call() throws Exception {

        long loopIterations = 0;
        Random rnd = new Random();

        AtomicBoolean keepRunning = new AtomicBoolean(true);
        Thread shutdownHook = new Thread(() -> {
            keepRunning.set(false);
            System.out.println("Stopping jload, please wait ...");
        });
        Runtime.getRuntime().addShutdownHook(shutdownHook);

        if(!testDisk && !testProcessor) {
            System.out.println("No load tests enabled.");
            System.out.println("Use the -h flag for usage information.");
            return 0;
        }

        do {

            ExecutorService pool = Executors.newFixedThreadPool(numberOfThreads * 2);

            for (int d = 0; d < numberOfThreads; d++) {
                if (testDisk) {
                    pool.execute(new DiskThread(numberOfIterations, weight));
                }
                if (testProcessor) {
                    pool.execute(new ProcessorThread(numberOfIterations, weight));
                }
            }

            pool.shutdown();
            pool.awaitTermination(1, TimeUnit.HOURS);

            loopIterations++;
            if (loopIterations >= numberOfRepeats && numberOfRepeats > 0) {
                keepRunning.set(false);
            } else {
                // Cool off
                int sleepTime = ((rnd.nextInt(10) + 1) * 10) * 1000;
                log.info("Cooling off for " + sleepTime + "ms ...");
                sleep(sleepTime);
            }

        } while (keepRunning.get());

        return 0;
    }

}
