/*
   Copyright 2020 mark.nellemann@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package biz.nellemann.jload;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.time.Instant;

class ProcessorThread extends Thread {

    private final static Logger log = LoggerFactory.getLogger(ProcessorThread.class);
    final long iterations;
    final long weight;

    final int maxSleep = 100;
    final int minSleep = 0;


    ProcessorThread(final long iterations, final long weight) {
        this.iterations = iterations;
        this.weight = weight;
    }


    @Override
    public void run() {

        double rampUpIterations = Math.min(iterations*0.2, maxSleep);
        double rampDownIterations = Math.min(iterations*0.2, maxSleep);
        long sleep = (long) Math.min(rampUpIterations, maxSleep);

        log.info("run() => rampUps: {}, rampDowns: {}, iterations: {}, weight: {}, sleep: {},", rampUpIterations, rampDownIterations, iterations, weight, sleep);

        for(int i = 0; i < iterations; i++) {

            if(i <= rampUpIterations) {
                log.debug("{} - Ramping Up", i);
                sleep = Math.max(sleep-1, minSleep);
            } else if (i >= (iterations - rampDownIterations)) {
                log.debug("{} - Ramping down", i);
                sleep = Math.min(sleep+1, maxSleep);
            } else {
                log.debug("{} - Full go", i);
                sleep=minSleep;
            }

            try {
                findPrimes( (weight*1000) / Math.max(5,sleep), sleep);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }


    protected void findPrimes(long weight, long sleep) throws InterruptedException {
        long i, primes = 0;
        Instant start = Instant.now();
        for (long num = (weight*100); num <= (weight*150); ++num) {
            for (i = 2; (i <= num) && (num % i != 0); ++i);
            if (i == num) {
                ++primes;
                sleep(sleep);
            }
        }
        Instant end = Instant.now();
        log.info("findPrimes() - {} primes in {} seconds - weight: {}, sleep: {}", primes, Duration.between(start, end).getSeconds(), weight, sleep);
    }

}
