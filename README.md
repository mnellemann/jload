# jLoad

Small utility that puts semi-random load on CPU and disk.

## Requirements

You need Java (JRE) version 8 or later to run jload.

## Usage Instructions

- Install the jload package (*.deb*, *.rpm* or *.jar*) from [downloads](https://bitbucket.org/mnellemann/jload/downloads/) or compile from source.
- Run **/opt/jload/bin/jload**, if installed from package
- Or as **java -jar /path/to/jload.jar**

To change the temporary directory where disk-load files are written to, use the *-Djava.io.tmpdir=/mytempdir* option.

```shell
Usage: jload [-hV] [--[no-]disk] [--[no-]processor] [-dw=num] [-i=num]
             [-pc=num] [-r=num] [-s=num] [-t=num]
Generate random load on CPU and Disk.
  -r, --repeats=num        Repetitions or -1 for continued (default: 50).
  -t, --threads=num        Threads to start (processor and disk) (default: 1).
  -i, --iterations=num     Iterations for each thread (default: 5).
      --[no-]processor     Disable load on processor(s).
      --[no-]disk          Disable load on disk(s).
      -dw, --disk-writes=num
                           Disk writes per. iteration (default: 1000).
      -pc, --processor-calculations=num
                           CPU calculations per. iteration (default: 1000).
  -h, --help               Show this help message and exit.
  -V, --version            Print version information and exit.
```

## Development Information

You need Java (JDK) version 8 or later to build jload.

### Build & Test

Use the gradle build tool, which will download all required dependencies:

```shell
./gradlew clean build run
```

### Container Build

To build as a docker container:

```shell
# Prerequisite:  ./gradlew build
docker build -t mnellemann/jload:latest .
```

To execute container:
```shell
docker run --rm mnellemann/jload:latest --help
```

### Multiarch Container Build

Prerequisite: run ```./gradlew build``` to have the Java artifact ready, so it can be copied into the containers during build.

```shell
export DOCKER_BUILDKIT=1

# Build and push x86_64 target
docker build --build-arg TARGET=x86_64 -t mnellemann/jload:x86_64-linux-latest .
docker push mnellemann/jload:x86_64-linux-latest

# Build and push ppc64le target
docker build --platform ppc64le --build-arg TARGET=ppc64le -t mnellemann/jload:ppc64le-linux-latest .
docker push mnellemann/jload:ppc64le-linux-latest

# Build and push aarch64 target
docker build --platform aarch64 --build-arg TARGET=aarch64 -t mnellemann/jload:aarch64-linux-latest .
docker push mnellemann/jload:aarch64-linux-latest

# Create and push a manifest (docker experimental feature)
# This enables you to pull mnellemann/jload:latest and get the right image depending on your platform
docker manifest create --amend \
  mnellemann/jload:latest \
  mnellemann/jload:x86_64-linux-latest \
  mnellemann/jload:ppc64le-linux-latest \
  mnellemann/jload:aarch64-linux-latest
docker manifest push mnellemann/jload:latest
```
